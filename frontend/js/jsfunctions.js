var serverAddress = 'http://localhost:8080/game';
var boardPictures = new Array();
var percentLoaded = 0;
var guessingPictureNumber = 0;
var score = 0;
var playable = false;
var arrayOfCodes, arrayOfCodes2;
var pictureFolder;
var testName="TestName"

function connectToGame(testName){
	client = Stomp.over(new SockJS(serverAddress));
	client.connect({userName:testName}, function (frame) {
		$("#testNameForm").html("Wellcome " + testName);
		client.subscribe("/topic/givemescore", function (data) {
			sendGameScore();
	    });
		client.subscribe("/user/topic/newscore", function (data) {
			var received = JSON.parse(data.body);
			$("#playerPoints").html(received.score);
	    });
	    client.subscribe("/topic/servermessage", function (data) {
	    	$("#gameBoard").load("html/boards/board1.html");
	    	var received = JSON.parse(data.body);
	    	arrayOfCodes = received.arrayOfCodes;
	    	arrayOfCodes2 = received.arrayOfCodes2;
	    	playable = false;
	    	guessingPictureNumber = 0;
	    	score = 0;
	    	$("#score").html(score);
	    	if (arrayOfCodes[0] < 100){
	    		pictureFolder = "000";
	    	}
	    	document.getElementById("guessingPictureImg").src="pictures/loading.jpg";
	    	preloadBoardPictures();
	    	//showPictureCode(JSON.parse(data.body));
	    	//showPictureCode();
	    	setTimeout(function(){
	    		playable = true;
	    		loadPicturesInPlayArea();
	    		loadGuessingPicture();
	    		}, 3000);
	    });
	    client.subscribe("/topic/hiscore", function (data){
	    	var received = JSON.parse(data.body);
	    	//$("#test").html(received.playerName[0]);
	    	$("#hiScoreListName").html("");
	    	$("#hiScoreListScore").html("");
	    	for(i=0; i<10; i++){
	    		$("#hiScoreListName").append('<li>' + received.playerName[i] + '</li>');
	    		$("#hiScoreListScore").append("<li>" + received.playerScore[i] + "</li>");
	    	}
	    });
	});
}

//delete function
function showPictureCode(){
	$("#pictureCode").html(arrayOfCodes[0]);
}

function sendGameScore(){
	client.send("/app/takescore", {}, JSON.stringify({
	    score: score
	}));
}

function loadPicturesInPlayArea(){
	for(i=0;i<100;i++){
		//$("#"+i).attr("src", ("pictures/" + data.arrayOfCodes[i] + ".jpg"));
		$("#"+i).attr("src", boardPictures[arrayOfCodes[i]].src);
		//document.getElementById(i).appendChild(boardPictures[data.arrayOfCodes[i]]);
		//document.getElementById(i).src=boardPictures[data.arrayOfCodes[i]];
		//$('#pictureCode').append(data.arrayOfCodes[i]);
	}
}

function loadGuessingPicture(){
	document.getElementById("guessingPictureImg").src="pictures/" + pictureFolder + "/" +  arrayOfCodes2[guessingPictureNumber] + ".jpg";
}

//this maybe need speed upgrade
function preloadBoardPictures(){
    percentLoaded = 0;
	for(i=0;i<100;i++){
    	boardPictures[i] = new Image();
    	boardPictures[i].onloadend = function(){
    		percentLoaded++;
    		$("#percentLoaded").html(percentLoaded);
		}
    	boardPictures[i].src = "pictures/" + pictureFolder + "/" + i + ".jpg";
    }
}

function clickedPicture(pictureNumber){
	if (playable){
		if(arrayOfCodes[pictureNumber] == arrayOfCodes2[guessingPictureNumber]){
			document.getElementById(pictureNumber).src="pictures/00000.jpg";
			score++;
			$("#score").html(score);
			guessingPictureNumber++;
			loadGuessingPicture();
		}
	}
}