package picturing.controller;

import java.util.LinkedList;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import picturing.domain.HiScore;
import picturing.domain.PlayerSession;

@Controller
public class ConnectEvents implements ApplicationListener<SessionConnectEvent>{

	private static LinkedList<PlayerSession> playerSessions = new LinkedList<PlayerSession>();

	public void onApplicationEvent(SessionConnectEvent event) {
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		String userName = sha.getNativeHeader("userName").get(0);
		PlayerSession playerSession = new PlayerSession(sha.getSessionId(), userName);
		playerSessions.add(playerSession);
        //System.out.println("Connect event [sessionId: " + sha.getSessionId() +"; userName: "+ userName + " ]");
	}
	
	@EventListener
	public void onApplicationEvent(SessionConnectedEvent event) {
/*		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		String userId = sha.getSessionId();
		for(PlayerSession playerSession : playerSessions){
			if(playerSession.getSessionId() == userId){
				System.out.println("U listi!!");
			}
		}*/
	}
	
	@EventListener
	public void onApplicationEvent(SessionDisconnectEvent event) {
		StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		//System.out.println(sha.getSessionId());
	    PlayerSession playerSessionForRemoving = new PlayerSession();
		for(PlayerSession playerSession : playerSessions){
	    	if(playerSession.getSessionId() == sha.getSessionId()){
	    		playerSessionForRemoving = playerSession;
	    	}
	    }
		//System.out.println("Player " + playerSessionForRemoving.getUserName() + " has been removed!");
		playerSessions.remove(playerSessionForRemoving);
	}
	
	public static int setAndGetPlayerScore(String sessionId, int score){
		int newScore = 0;
		for(PlayerSession playerSession : playerSessions){
			if(playerSession.getSessionId() == sessionId){
				newScore = playerSession.getScore() + score;
				playerSession.setScore(newScore);
			}
		}
		return newScore;
	}
	
	public static HiScore getHiScore() {
		String[] listOfNames = {"None yet!", "None yet!", "None yet!", "None yet!", "None yet!", 
				"None yet!", "None yet!", "None yet!", "None yet!", "None yet!" };
		int[] listOfScores = new int[10];
		for(PlayerSession playerSession : playerSessions) {
			for(int i=0; i<10; i++) {
				if(listOfScores[i]<playerSession.getScore()) {
					if(i<9) {
						for(int j=8; j>i-1; j--) {
							listOfScores[j+1] = listOfScores[j];
							listOfNames[j+1] = listOfNames[j];
						}
					listOfScores[i] = playerSession.getScore();
					listOfNames[i] = playerSession.getUserName();
					break;
					}
					else {
						listOfScores[i] = playerSession.getScore();
						listOfNames[i] = playerSession.getUserName();
					}
				}
			}
		}
		//for(int i=0; i<10; i++) {System.out.println(listOfNames[i]+ listOfScores[i]);}
		return new HiScore(listOfNames, listOfScores);
	}
}
