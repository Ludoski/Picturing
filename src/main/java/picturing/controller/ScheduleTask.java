package picturing.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import picturing.controller.ConnectEvents;
import picturing.domain.CodesForPictures;
import picturing.domain.Game;
import picturing.domain.HiScore;
import picturing.domain.ReceivedScore;

@Controller
@EnableScheduling
class ScheduleTask {
	
	CodesForPictures codesForPictures = new CodesForPictures();
	
    @Autowired
    private SimpMessagingTemplate template;
    
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
 
    @Scheduled(fixedDelay = 20000)
    public void exchangeGameData(){
    	sendScoreRequest();
    	sendGame();
    }
    
    @Scheduled(fixedDelay = 15000)
    public void sendHiScore() {
    	HiScore hiScore = ConnectEvents.getHiScore();
    	//String[] test = hiScore.getPlayerName();
    	//System.out.println(test[0]);
    	template.convertAndSend("/topic/hiscore", hiScore);
    }

    public void sendScoreRequest(){
    	template.convertAndSend("/topic/givemescore", "");
    }
    
    @MessageMapping("/takescore")
    //@SendTo("/topic/newscore")
    public void sendScore(SimpMessageHeaderAccessor headerAccessor, ReceivedScore receivedScore){
    	String sessionId = headerAccessor.getSessionId();
    	int newScore = ConnectEvents.setAndGetPlayerScore(sessionId, receivedScore.getScore());
    	//System.out.println(sessionId + "    " + newScore);
    	messagingTemplate.convertAndSendToUser(sessionId, "/topic/newscore", new ReceivedScore(newScore), createHeaders(sessionId));
    	//return new ReceivedScore(newScore);
    }
    
    public void sendGame() {
    	template.convertAndSend("/topic/servermessage", new Game(codesForPictures.getArrayOfCodes(),codesForPictures.getArrayOfCodes2()));
    }
    
    private MessageHeaders createHeaders(String sessionId) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }
 }