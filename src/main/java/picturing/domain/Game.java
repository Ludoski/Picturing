package picturing.domain;


//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;

public class Game {
	
	//arrayOfCodes = image codes for matrix
	//arrayOfCodes2 = image codes for guessing one by one
	private Integer[] arrayOfCodes = new Integer[100];
	private Integer[] arrayOfCodes2 = new Integer[100];
	
	public Game(Integer[] arrayOfCodes, Integer[] arrayOfCodes2){
		this.arrayOfCodes = arrayOfCodes;
		this.arrayOfCodes2 = arrayOfCodes2;
	}
	
	public Integer[] getArrayOfCodes(){
		//Collections.shuffle(Arrays.asList(arrayOfCodes));
		return arrayOfCodes;
	}
	
	public Integer[] getArrayOfCodes2(){
		//Collections.shuffle(Arrays.asList(arrayOfCodes2));
		return arrayOfCodes2;
	}
}
