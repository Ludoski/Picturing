package picturing.domain;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CodesForPictures {

	private Integer[] arrayOfCodes = new Integer[100];
	private Integer[] arrayOfCodes2 = new Integer[100];
	
	public CodesForPictures(){
		int number = -1;
		for(int i=0; i<100; i++){
			number++;
			arrayOfCodes[i] = number;
			arrayOfCodes2[i] = number;
		}
		//Collections.shuffle(Arrays.asList(arrayOfCodes));
		//Collections.shuffle(Arrays.asList(arrayOfCodes2));
	}
	
	public Integer[] getArrayOfCodes(){
		Collections.shuffle(Arrays.asList(arrayOfCodes));
		return arrayOfCodes;
	}
	
	public Integer[] getArrayOfCodes2(){
		Collections.shuffle(Arrays.asList(arrayOfCodes2));
		return arrayOfCodes2;
	}
}
