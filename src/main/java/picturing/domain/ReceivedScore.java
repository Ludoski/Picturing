package picturing.domain;


public class ReceivedScore {

	private int score;
	
	public ReceivedScore() {}
	
	public ReceivedScore(int score) {
		this.score = score;
	}
	
	public int getScore(){
		return score;
	}
}
