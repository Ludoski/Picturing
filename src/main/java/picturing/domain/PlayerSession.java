package picturing.domain;

public class PlayerSession {

	private String sessionId;
	private String userName;
	private int score = 0;
	
	public PlayerSession(String sessionId, String userName){
		this.sessionId = sessionId;
		this.userName = userName;
	}
	
	public PlayerSession(){
		
	}
	
	public void setSessionId(String sessionId){
		this.sessionId = sessionId;
	}
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public void setScore(int score){
		this.score = score;
	}
	
	public String getSessionId(){
		return sessionId;
	}
	
	public String getUserName(){
		return userName;
	}
	
	public int getScore(){
		return score;
	}
}
