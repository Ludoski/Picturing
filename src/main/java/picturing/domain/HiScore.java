package picturing.domain;

public class HiScore {

	private String[] playerName;
	private int[] playerScore;
	
	public HiScore() {
	}
	
	public HiScore(String[] playerName, int[] playerScore) {
		this.playerName = playerName;
		this.playerScore = playerScore;
	}
	
	public void setPlayerName(String[] playerName) {
		this.playerName = playerName;
	}
	
	public void setPlayerScore(int[] playerScore) {
		this.playerScore = playerScore;
	}
	
	public String[] getPlayerName() {
		return playerName;
	}
	
	public int[] getPlayerScore() {
		return playerScore;
	}
}
